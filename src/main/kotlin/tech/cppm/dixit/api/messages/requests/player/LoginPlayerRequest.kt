package tech.cppm.dixit.api.messages.requests.player

import com.fasterxml.jackson.annotation.JsonTypeName
import tech.cppm.dixit.api.messages.ApiRequest
import tech.cppm.dixit.api.messages.requests.PlayerRequest

@JsonTypeName("players.register")
class LoginPlayerRequest(
    params: Params,
    id: String?
): PlayerRequest<LoginPlayerRequest.Params>(
    method = "players.login",
    params, id
) {
    data class Params(
        val alias: String,
        val password: String,
    ) : ApiRequest.Params()
}