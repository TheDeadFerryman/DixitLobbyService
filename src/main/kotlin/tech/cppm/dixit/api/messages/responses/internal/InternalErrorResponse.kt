package tech.cppm.dixit.api.messages.responses.internal

import tech.cppm.dixit.api.messages.responses.ErrorCode
import tech.cppm.dixit.api.messages.responses.ErrorResponse

class InternalErrorResponse(
    data: Any? = null
) : ErrorResponse(
    error = ApiError(
        code = ErrorCode.INTERNAL_ERROR,
        data
    ),
    id = null
)