package tech.cppm.dixit.util

import kotlin.reflect.KClass
import kotlin.reflect.KClassifier
import kotlin.reflect.KType
import kotlin.reflect.KTypeParameter

object KReflect {
    fun publicTypeName(type: KType) : String {
        val clas = classifierName(type.classifier)

        val argTypes = type.arguments
            .map { it.type }
            .map { it?.let { it1 -> publicTypeName(it1) } }
            .map { it ?: "Any" }

        val argStr = if (argTypes.isNotEmpty()) {
            "<${argTypes.joinToString(", ")}>"
        } else {
            ""
        }

        return "$clas$argStr"
    }

    private fun classifierName(classifier: KClassifier?) =
        when (classifier) {
            is KClass<*> -> classifier.simpleName ?: "Any"
            is KTypeParameter -> classifier.name
            else -> "Any"
        }
}