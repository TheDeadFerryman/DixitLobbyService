package tech.cppm.dixit.db.documents

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Player(
    val alias: String,
    val password: String
) {
    @Id
    lateinit var id: String
}
