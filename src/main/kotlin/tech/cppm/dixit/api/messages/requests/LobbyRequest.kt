package tech.cppm.dixit.api.messages.requests

import tech.cppm.dixit.api.messages.ApiRequest

abstract class LobbyRequest<R : ApiRequest.Params>(
    method: String,
    params: R,
    id: String?
) : ApiRequest<R>(method, params, id)