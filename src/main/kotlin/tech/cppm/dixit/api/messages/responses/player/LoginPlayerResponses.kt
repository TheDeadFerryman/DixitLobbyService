package tech.cppm.dixit.api.messages.responses.player

import tech.cppm.dixit.api.messages.responses.ErrorCode
import tech.cppm.dixit.api.messages.responses.ErrorResponse
import tech.cppm.dixit.api.messages.responses.SuccessResponse
import tech.cppm.dixit.db.documents.Player

sealed class LoginPlayerResponses {
    class LoginSuccess(
        result: Player,
        id: String?
    ) : SuccessResponse<Player>(result, id)

    class ErrorInvalidCredentials(
        id: String?
    ) : ErrorResponse(
        id, error = ApiError(ErrorCode.LOGIN_PLAYER_INVALID_CRED)
    )
}
