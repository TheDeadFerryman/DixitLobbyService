package tech.cppm.dixit.control.amqp

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessageBuilder
import org.springframework.stereotype.Service
import tech.cppm.dixit.api.messages.ApiMessage
import tech.cppm.dixit.config.AMQPConfig
import tech.cppm.dixit.db.documents.Lobby
import tech.cppm.dixit.db.documents.Player
import tech.cppm.dixit.api.messages.ApiRequest
import tech.cppm.dixit.control.messages.ControlMessage

class ControlDispatcher private constructor(
    private val entityManager: AMQPEntityManager,
    private val objectMapper: ObjectMapper,
    private val template: AmqpTemplate,
    val lobby: Lobby,
) {
    @Service
    class Factory(
        private val entityManager: AMQPEntityManager,
        private val objectMapper: ObjectMapper,
        private val template: AmqpTemplate,
    ) {
        fun forLobby(lobby: Lobby) = ControlDispatcher(
            entityManager,
            objectMapper,
            template,
            lobby
        )
    }

    fun sendBroadcast(message: ApiMessage) {
        val binding = entityManager.lobbyBroadcastBinding(lobby)

        return template.send(
            binding.exchange,
            binding.route,
            buildMessage(message)
        )
    }

    fun sendDirect(message: ApiMessage, receiver: Player) {
        val binding = entityManager.lobbyPlayerBinding(lobby, receiver)

        return template.send(
            binding.exchange,
            binding.route,
            buildMessage(message)
        )
    }

    private fun buildMessage(message: ApiMessage): Message? {
        val data = objectMapper.writeValueAsBytes(message)

        return MessageBuilder.withBody(data)
            .setContentType(AMQPConfig.MESSAGE_MEDIA_TYPE)
            .build()
    }
}