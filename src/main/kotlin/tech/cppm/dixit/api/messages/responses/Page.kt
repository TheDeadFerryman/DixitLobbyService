package tech.cppm.dixit.api.messages.responses

data class Page(
    val page: Int,
    val pageSize: Int
)
