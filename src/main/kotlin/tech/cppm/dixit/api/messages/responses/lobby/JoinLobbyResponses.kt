package tech.cppm.dixit.api.messages.responses.lobby

import tech.cppm.dixit.db.documents.Player
import tech.cppm.dixit.api.messages.responses.ErrorCode
import tech.cppm.dixit.api.messages.responses.ErrorResponse
import tech.cppm.dixit.api.messages.responses.SuccessResponse

sealed class JoinLobbyResponses {
    class JoinedSuccess(
        result: Result,
        id: String?
    ) : SuccessResponse<JoinedSuccess.Result>(result, id) {
        data class Result(
            val players: List<Player>
        )
    }

    class ErrorInvalidPasskey(
        id: String?
    ) : ErrorResponse(
        id,
        error = ApiError(
            code = ErrorCode.JOIN_LOBBY_INVALID_PASSKEY
        ),
    )

    class ErrorInvalidId(
        id: String?
    ) : ErrorResponse(
        id,
        error = ApiError(
            code = ErrorCode.JOIN_LOBBY_INVALID_ID
        )
    )
}