package tech.cppm.dixit.db.documents

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import kotlin.random.Random

@Document
data class Lobby(
    val name: String,
    val passkey: String?,
    val region: LobbyRegion,
    val status: LobbyStatus,
    val seed: Int,
    @DBRef val players: List<Player>
) {
    companion object {
        fun create(
            name: String,
            passkey: String?,
            region: LobbyRegion,
            status: LobbyStatus,
            players: List<Player>
        ) = Lobby(
            name, passkey,
            region, status,
            seed = Random.nextInt(),
            players
        )
    }

    @Id
    lateinit var id: String

    fun addPlayer(player: Player): Lobby {
        val lobby = Lobby(
            name, passkey, region, status, seed,
            players = (players + listOf(player))
        )

        lobby.id = id

        return lobby
    }

    fun removePlayer(player: Player): Lobby {
        val lobby = Lobby(
            name, passkey, region, status, seed,
            players = players.filterNot { it.id == player.id }
        )

        lobby.id = id

        return lobby
    }

    fun setStatus(status: LobbyStatus): Lobby {
        return Lobby(
            name, passkey, region, status, seed,
            players
        )
    }
}