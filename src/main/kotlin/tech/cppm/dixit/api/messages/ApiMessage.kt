package tech.cppm.dixit.api.messages

abstract class ApiMessage {
    val jsonrpc: String = "2.0"
}