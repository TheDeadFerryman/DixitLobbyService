package tech.cppm.dixit.db.repo

import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import tech.cppm.dixit.db.documents.Lobby
import tech.cppm.dixit.db.documents.LobbyRegion

@Suppress("SpringDataRepositoryMethodParametersInspection")
@Repository
interface LobbyRepo : MongoRepository<Lobby, String>, PagingAndSortingRepository<Lobby, String> {
    fun getById(id: String): Lobby?

    fun existsByName(name: String): Boolean

    fun findAllByNameContains(name: String): List<Lobby>

    fun findAllByNameContains(name: String, pageable: Pageable): List<Lobby>

    fun findAllByRegionAndNameContains(region: LobbyRegion, name: String): List<Lobby>

    fun findAllByRegionAndNameContains(region: LobbyRegion, name: String, pageable: Pageable): List<Lobby>

    fun findAllByRegion(region: LobbyRegion): List<Lobby>

    fun findAllByRegion(region: LobbyRegion, pageable: Pageable): List<Lobby>
}