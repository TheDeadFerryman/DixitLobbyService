package tech.cppm.dixit.control.messages.requests

import com.fasterxml.jackson.annotation.JsonTypeName
import tech.cppm.dixit.control.messages.ControlMessage

@JsonTypeName("control.game.start")
class StartGameRequest(
    params: Params
) : ControlMessage<StartGameRequest.Params>(
    method = "control.game.start",
    params
) {
    data class Params(
        val creatorId: String,
        val lobbyId: String
    ) : ControlMessage.Params()
}