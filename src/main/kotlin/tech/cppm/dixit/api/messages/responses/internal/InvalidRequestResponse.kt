package tech.cppm.dixit.api.messages.responses.internal

import tech.cppm.dixit.api.messages.responses.ErrorCode
import tech.cppm.dixit.api.messages.responses.ErrorResponse

class InvalidRequestResponse(
    data: Any? = null
) : ErrorResponse(
    id = null,
    error = ApiError(
        code = ErrorCode.REQUEST_INVALID,
        data
    )
)