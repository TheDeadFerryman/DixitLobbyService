package tech.cppm.dixit.api.messages.responses

import com.fasterxml.jackson.annotation.JsonValue

enum class ErrorCode(
    @JsonValue val code: Int,
    val message: String
) {
    // region RPC std
    PARSE_FAILED(-32700, "Invalid JSON data"),
    REQUEST_INVALID(-32600, "Invalid request object"),
    NO_METHOD(-32601, "Method not found"),
    PARAMS_INVALID(-32602, "Invalid method params"),
    INTERNAL_ERROR(-32603, "Internal error occured"),
    // endregion

    // region 'lobbies.*'

    CREATE_LOBBY_EXISTS(1, "Lobby already exists"),
    CREATE_LOBBY_INVALID_PLAYER(7, "Invalid player id"),

    JOIN_LOBBY_INVALID_PASSKEY(2, "Invalid passkey"),
    JOIN_LOBBY_INVALID_ID(3, "Invalid lobby id"),
    JOIN_LOBBY_INVALID_PLAYER(7, "Invalid player id"),

    DROP_LOBBY_INVALID_ID(3, "Invalid lobby id"),

    // endregion

    // region 'players.*'

    REGISTER_PLAYER_EXISTS(5, "Player already exists"),

    LOGIN_PLAYER_INVALID_CRED(6, "Invalid credentials!")

    // endregion 'players.*'
}