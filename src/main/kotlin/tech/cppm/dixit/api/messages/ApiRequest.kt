package tech.cppm.dixit.api.messages

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonTypeInfo
import tech.cppm.dixit.api.messages.requests.RequestTypes


@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    property = "method"
)
@RequestTypes
@JsonIgnoreProperties(ignoreUnknown = true)
abstract class ApiRequest<ParamsType : ApiRequest.Params>(
    val method: String,
    val params: ParamsType,
    val id: String?,
) : ApiMessage() {
    abstract class Params

    open fun validate(): Boolean {
        return jsonrpc == "2.0"
    }
}