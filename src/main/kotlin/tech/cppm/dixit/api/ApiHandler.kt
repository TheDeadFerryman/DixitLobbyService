package tech.cppm.dixit.api

import org.springframework.stereotype.Service
import tech.cppm.dixit.api.messages.ApiRequest
import tech.cppm.dixit.api.messages.ApiResponse
import tech.cppm.dixit.api.messages.requests.LobbyRequest
import tech.cppm.dixit.api.messages.responses.internal.NoMethodResponse
import tech.cppm.dixit.api.handlers.LobbyHandler
import tech.cppm.dixit.api.handlers.PlayerHandler
import tech.cppm.dixit.api.messages.requests.EmptyRequest
import tech.cppm.dixit.api.messages.requests.PlayerRequest
import tech.cppm.dixit.api.messages.responses.EmptyResponse
import tech.cppm.dixit.control.amqp.ControlDispatcher
import tech.cppm.dixit.db.repo.LobbyRepo

@Service
class ApiHandler(
    private val lobbyHandler: LobbyHandler,
    private val playerHandler: PlayerHandler,
    private val dispatcherFactory: ControlDispatcher.Factory,
    private val lobbyRepo: LobbyRepo
) {
    fun handle(request: ApiRequest<*>): ApiResponse =
        when(request) {
            is LobbyRequest<*> -> lobbyHandler.handle(request)
            is PlayerRequest<*> -> playerHandler.handle(request)
            is EmptyRequest -> handleEmtpyRequest(request)
            else -> NoMethodResponse(request.method)
        }

    fun handleEmtpyRequest(request: EmptyRequest) : EmptyResponse {
        dispatcherFactory.forLobby(lobbyRepo.findAll().first())
            .sendBroadcast(EmptyRequest("test"))

        return EmptyResponse(request.id)
    }
}