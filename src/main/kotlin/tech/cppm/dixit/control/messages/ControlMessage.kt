package tech.cppm.dixit.control.messages

import tech.cppm.dixit.api.messages.ApiRequest

abstract class ControlMessage<R : ControlMessage.Params>(
    method: String,
    params: R
) : ApiRequest<R>(
    method, params,
    id = null
) {
    abstract class Params : ApiRequest.Params()
}