package tech.cppm.dixit

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties
@ConfigurationPropertiesScan("tech.cppm.dixit.config")
class DixitLobbyServer

fun main() {
    runApplication<DixitLobbyServer>()
}
