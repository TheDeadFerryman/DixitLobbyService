package tech.cppm.dixit.control.amqp

data class AMQPBinding(
    val exchange: String,
    val route: String
)
