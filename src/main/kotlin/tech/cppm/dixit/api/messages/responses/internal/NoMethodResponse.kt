package tech.cppm.dixit.api.messages.responses.internal

import tech.cppm.dixit.api.messages.responses.ErrorCode
import tech.cppm.dixit.api.messages.responses.ErrorResponse

class NoMethodResponse(
    data: Any
) : ErrorResponse(
    id = null,
    error = ApiError(
        code = ErrorCode.NO_METHOD,
        data
    )
)