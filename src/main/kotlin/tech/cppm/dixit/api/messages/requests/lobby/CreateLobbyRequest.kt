package tech.cppm.dixit.api.messages.requests.lobby

import com.fasterxml.jackson.annotation.JsonTypeName
import tech.cppm.dixit.api.messages.ApiRequest
import tech.cppm.dixit.api.messages.requests.LobbyRequest
import tech.cppm.dixit.db.documents.LobbyRegion

@JsonTypeName("lobbies.create")
class CreateLobbyRequest(
    params: Params,
    id: String?
) : LobbyRequest<CreateLobbyRequest.Params>(
    method = "lobbies.create",
    params, id
) {
    data class Params(
        val name: String,
        val passkey: String,
        val region: LobbyRegion,
        val creatorId: String,
    ) : ApiRequest.Params()
}