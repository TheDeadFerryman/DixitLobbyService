package tech.cppm.dixit.control.messages.requests

import com.fasterxml.jackson.annotation.JsonTypeName
import tech.cppm.dixit.control.messages.ControlMessage

@JsonTypeName("control.game.end")
class EndGameRequest(
    params: Params
) : ControlMessage<EndGameRequest.Params>(
    method = "control.game.end",
    params
) {
    data class Params(
        val lobbyId: String,
        val playerId: String
    ) : ControlMessage.Params()
}