package tech.cppm.dixit.control.amqp

import org.springframework.amqp.core.AmqpAdmin
import org.springframework.amqp.core.BindingBuilder
import org.springframework.amqp.core.Queue
import org.springframework.amqp.core.TopicExchange
import org.springframework.stereotype.Service
import tech.cppm.dixit.db.documents.Lobby
import tech.cppm.dixit.db.documents.Player

@Service
class AMQPEntityManager(
    private val admin: AmqpAdmin
){
    fun lobbyExchange(lobby: Lobby) = TopicExchange(lobby.id).also { admin.declareExchange(it) }

    fun lobbyPlayerBinding(lobby: Lobby, player: Player): AMQPBinding {
        val binding = BindingBuilder
            .bind(playerQueue(player))
            .to(lobbyExchange(lobby))
            .with(playerBindingRoute(player))
            .also { admin.declareBinding(it) }

        return AMQPBinding(
            exchange = binding.exchange,
            route = binding.routingKey
        )
    }

    fun lobbyBroadcastBinding(lobby: Lobby): AMQPBinding {
        val lobbyExchange = lobbyExchange(lobby)

        lobby.players.forEach { player ->
            BindingBuilder
                .bind(playerQueue(player))
                .to(lobbyExchange)
                .with(broadcastBindingRoute(lobby))
                .also { admin.declareBinding(it) }
        }


        return AMQPBinding(
            exchange = lobbyExchange.name,
            route = "#"
        )
    }

    fun playerQueue(player: Player) =
        Queue(
            playerBindingRoute(player),
            false
        ).also { admin.declareQueue(it) }

    fun playerBindingRoute(player: Player) = player.id

    fun broadcastBindingRoute(lobby: Lobby) = "#"
}