package tech.cppm.dixit.db.documents

enum class LobbyRegion {
    EUROPE,
    ASIA,
    NORTH_AMERICA,
    SOUTH_AMERICA,
    AUSTRALIA
}