package tech.cppm.dixit.api.messages.requests.player

import com.fasterxml.jackson.annotation.JsonTypeName
import tech.cppm.dixit.api.messages.ApiRequest
import tech.cppm.dixit.api.messages.requests.PlayerRequest

@JsonTypeName("players.register")
class RegisterPlayerRequest(
    params: Params,
    id: String?
): PlayerRequest<RegisterPlayerRequest.Params>(
    method = "players.register",
    params, id
) {
    data class Params(
        val alias: String,
        val password: String,
    ) : ApiRequest.Params()
}