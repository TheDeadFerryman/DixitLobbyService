package tech.cppm.dixit.api.handlers

import org.springframework.stereotype.Service
import tech.cppm.dixit.api.messages.ApiResponse
import tech.cppm.dixit.api.messages.requests.LobbyRequest
import tech.cppm.dixit.api.messages.requests.lobby.CreateLobbyRequest
import tech.cppm.dixit.api.messages.requests.lobby.FindLobbyRequest
import tech.cppm.dixit.api.messages.requests.lobby.JoinLobbyRequest
import tech.cppm.dixit.api.messages.responses.internal.NoMethodResponse
import tech.cppm.dixit.api.messages.responses.lobby.CreateLobbyResponses
import tech.cppm.dixit.api.messages.responses.lobby.FindLobbyResponse
import tech.cppm.dixit.api.messages.responses.lobby.JoinLobbyResponses
import tech.cppm.dixit.api.handlers.lobby.CreateLobbyProcessor
import tech.cppm.dixit.api.handlers.lobby.FindLobbyProcessor
import tech.cppm.dixit.api.handlers.lobby.JoinLobbyProcessor

@Service
class LobbyHandler(
    private val findLobbyProcessor: FindLobbyProcessor,
    private val createLobbyProcessor: CreateLobbyProcessor,
    private val joinLobbyProcessor: JoinLobbyProcessor
) {
    fun handle(request: LobbyRequest<*>): ApiResponse =
        when (request) {
            is FindLobbyRequest -> findLobby(request.params, request.id)
            is CreateLobbyRequest -> createLobby(request.params, request.id)
            is JoinLobbyRequest -> joinLobby(request.params, request.id)
            else -> NoMethodResponse(request.method)
        }

    private fun createLobby(params: CreateLobbyRequest.Params, id: String?) =
        try {
            val lobbyId = createLobbyProcessor(params)

            CreateLobbyResponses.CreatedSuccess(
                result = lobbyId, id
            )
        } catch (e: CreateLobbyProcessor.LobbyExistsException) {
            CreateLobbyResponses.LobbyAlreadyExistsError(id)
        } catch (e: CreateLobbyProcessor.InvalidPlayerException) {
            CreateLobbyResponses.InvalidPlayerError(id)
        }

    private fun findLobby(params: FindLobbyRequest.Params, id: String?): ApiResponse {
        val results = findLobbyProcessor(params).map {
            FindLobbyResponse.LobbyResult(
                id = it.id,
                name = it.name,
                region = it.region
            )
        }

        return FindLobbyResponse(results, id)
    }

    private fun joinLobby(params: JoinLobbyRequest.Params, id: String?) =
        try {
            val result = joinLobbyProcessor(params)

            JoinLobbyResponses.JoinedSuccess(result, id)
        } catch (e: JoinLobbyProcessor.InvalidIdError) {
            JoinLobbyResponses.ErrorInvalidId(id)
        } catch (e: JoinLobbyProcessor.InvalidPasskeyError) {
            JoinLobbyResponses.ErrorInvalidPasskey(id)
        }
}