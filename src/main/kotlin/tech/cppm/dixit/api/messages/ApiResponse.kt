package tech.cppm.dixit.api.messages

abstract class ApiResponse(
    val id: String?
) : ApiMessage()