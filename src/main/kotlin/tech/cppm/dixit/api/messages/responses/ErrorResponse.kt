package tech.cppm.dixit.api.messages.responses

import tech.cppm.dixit.api.messages.ApiResponse

abstract class ErrorResponse(
    id: String?,
    val error: ApiError
) : ApiResponse(id) {
    open class ApiError(
        val code: ErrorCode,
        val data: Any? = null,
        val message: String = code.message
    )
}