package tech.cppm.dixit.api.messages.responses.lobby

import tech.cppm.dixit.db.documents.LobbyRegion
import tech.cppm.dixit.api.messages.responses.SuccessResponse

class FindLobbyResponse(
    result: List<LobbyResult>,
    id: String?
) : SuccessResponse<List<FindLobbyResponse.LobbyResult>>(
    result, id
) {
        data class LobbyResult(
            val id: String,
            val name: String,
            val region: LobbyRegion
        )
}