package tech.cppm.dixit.api.handlers

import org.springframework.stereotype.Service
import tech.cppm.dixit.api.messages.ApiResponse
import tech.cppm.dixit.api.messages.requests.PlayerRequest
import tech.cppm.dixit.api.messages.requests.player.LoginPlayerRequest
import tech.cppm.dixit.api.messages.requests.player.RegisterPlayerRequest
import tech.cppm.dixit.api.messages.responses.internal.NoMethodResponse
import tech.cppm.dixit.api.messages.responses.player.LoginPlayerResponses
import tech.cppm.dixit.api.messages.responses.player.RegisterPlayerResponses
import tech.cppm.dixit.db.documents.Player
import tech.cppm.dixit.db.repo.PlayerRepo

@Service
class PlayerHandler(
    val playerRepo: PlayerRepo
) {
    fun handle(request: PlayerRequest<*>): ApiResponse =
        when (request) {
            is RegisterPlayerRequest -> handleRegister(request)
            is LoginPlayerRequest -> handleLogin(request)
            else -> NoMethodResponse(request.method)
        }

    fun handleRegister(request: RegisterPlayerRequest): ApiResponse {
        if (playerRepo.existsByAlias(request.params.alias)) {
            return RegisterPlayerResponses.ErrorPlayerExists(request.id)
        }

        val player = playerRepo.save(
            Player(
                request.params.alias,
                request.params.password,
            )
        )

        return RegisterPlayerResponses.RegisterSuccess(player, request.id)
    }

    fun handleLogin(request: LoginPlayerRequest): ApiResponse {
        val player = playerRepo.findByAliasAndPassword(
            request.params.alias,
            request.params.password
        ) ?: return LoginPlayerResponses.ErrorInvalidCredentials(request.id)

        return LoginPlayerResponses.LoginSuccess(player, request.id)
    }
}