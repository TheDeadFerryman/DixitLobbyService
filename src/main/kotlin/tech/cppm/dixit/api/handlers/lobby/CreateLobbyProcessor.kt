package tech.cppm.dixit.api.handlers.lobby

import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import tech.cppm.dixit.db.documents.Lobby
import tech.cppm.dixit.db.repo.LobbyRepo
import tech.cppm.dixit.db.repo.PlayerRepo
import tech.cppm.dixit.api.messages.requests.lobby.CreateLobbyRequest
import tech.cppm.dixit.control.amqp.AMQPEntityManager
import tech.cppm.dixit.db.documents.LobbyStatus
import java.lang.Exception

@Service
class CreateLobbyProcessor(
    private val lobbyRepo: LobbyRepo,
    private val playerRepo: PlayerRepo,
    private val amqpEntityManager: AMQPEntityManager
) {
    class LobbyExistsException : Exception()

    class InvalidPlayerException : Exception()

    operator fun invoke(params: CreateLobbyRequest.Params): String {
        if (lobbyRepo.existsByName(params.name)) throw LobbyExistsException()

        val creator = playerRepo.findByIdOrNull(params.creatorId) ?: throw InvalidPlayerException()

        val lobby = lobbyRepo.insert(
            Lobby.create(
                name = params.name,
                region = params.region,
                status = LobbyStatus.CREATED,
                passkey = params.passkey,
                players = listOf(creator)
            )
        )

        amqpEntityManager.lobbyPlayerBinding(lobby, creator)

        return lobby.id
    }
}