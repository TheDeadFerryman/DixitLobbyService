package tech.cppm.dixit.config

import org.springframework.amqp.core.*
import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableRabbit
class AMQPConfig {
    companion object {
        const val MESSAGE_MEDIA_TYPE = "application/json"

        const val API_INCOME_QUEUE = "LobbyServerApiIncome"
        const val API_OUTCOME_QUEUE = "LobbyServerApiOutcome"
        const val API_OUTCOME_EXCHANGE = "LobbyServerApiOutcomeXchg"

        const val CONTROL_INCOME_QUEUE = "LobbyServerControlIncome"
    }

    @Bean
    fun rabbitTemplate(connectionFactory: ConnectionFactory): RabbitTemplate {
        val template = RabbitTemplate()

        template.connectionFactory = connectionFactory

        return template
    }

    @Bean("ApiInputQueue")
    fun serverApiInputQueue() = Queue(API_INCOME_QUEUE)

    @Bean("ControlInputQueue")
    fun serverControlInputQueue() = Queue(CONTROL_INCOME_QUEUE)

    @Bean("ApiOutputQueue")
    fun serverApiOutputQueue() = Queue(API_OUTCOME_QUEUE)

    @Bean("ApiOutputExchange")
    fun serverOutputExchange() = FanoutExchange(API_OUTCOME_EXCHANGE)

    @Bean("ApiOutputBinding")
    fun serverOutputBinding(): Binding =
        BindingBuilder
            .bind(serverApiOutputQueue())
            .to(serverOutputExchange())
}