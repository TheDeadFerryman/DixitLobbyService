package tech.cppm.dixit.api.handlers.lobby

import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import tech.cppm.dixit.db.repo.LobbyRepo
import tech.cppm.dixit.db.repo.PlayerRepo
import tech.cppm.dixit.api.messages.requests.lobby.JoinLobbyRequest
import tech.cppm.dixit.api.messages.responses.lobby.JoinLobbyResponses
import tech.cppm.dixit.control.amqp.AMQPEntityManager

@Service
class JoinLobbyProcessor(
    val lobbyRepo: LobbyRepo,
    val playerRepo: PlayerRepo,
    val amqpEntityManager: AMQPEntityManager
) {
    class InvalidIdError : Exception()
    class InvalidPasskeyError : Exception()
    class InvalidPlayerError : Exception()

    operator fun invoke(params: JoinLobbyRequest.Params): JoinLobbyResponses.JoinedSuccess.Result {
        val lobby = lobbyRepo.getById(params.lobbyId) ?: throw InvalidIdError()

        if (params.passkey != lobby.passkey) throw InvalidPasskeyError()

        val player = playerRepo.findByIdOrNull(params.playerId) ?: throw InvalidPlayerError()

        val savedLobby = lobbyRepo.save(
            lobby.addPlayer(player)
        )

        amqpEntityManager.lobbyPlayerBinding(lobby, player)

        return JoinLobbyResponses.JoinedSuccess.Result(
            players = savedLobby.players
        )
    }
}