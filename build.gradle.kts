import org.jetbrains.kotlin.config.KotlinCompilerVersion
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.10"

    id("org.springframework.boot") version "2.4.3"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"

    kotlin("plugin.spring") version "1.4.10"
    kotlin("plugin.serialization") version "1.4.10"

    kotlin("kapt") version "1.4.10"
}

group = "tech.cppm.dixit"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib", KotlinCompilerVersion.VERSION))
    implementation(kotlin("reflect", KotlinCompilerVersion.VERSION))

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactive:1.3.9")

    implementation("org.springframework.boot:spring-boot-starter:2.4.3")
    implementation("org.springframework.boot:spring-boot-starter-web:2.4.3")

    implementation("org.mongodb:mongodb-driver-core:4.1.0")
    implementation("org.mongodb:mongodb-driver-sync:4.1.0")
    implementation("org.springframework.data:spring-data-mongodb:3.0.4.RELEASE")

    implementation("com.fasterxml.jackson.core:jackson-annotations:2.11.3")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.11.3")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.11.3")

    implementation("com.rabbitmq:amqp-client:5.11.0")
    implementation("org.springframework.boot:spring-boot-starter-amqp:2.4.3")

    implementation("org.springframework.boot:spring-boot-devtools")

    kapt("org.springframework.boot:spring-boot-configuration-processor:2.4.3")

}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}