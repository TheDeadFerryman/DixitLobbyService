package tech.cppm.dixit.api.handlers.lobby

import org.springframework.stereotype.Service
import tech.cppm.dixit.db.repo.LobbyRepo
import tech.cppm.dixit.db.repo.PlayerRepo
import tech.cppm.dixit.api.messages.requests.lobby.DropLobbyRequest

@Service
class DropLobbyProcessor(
    val lobbyRepo: LobbyRepo,
    val playerRepo: PlayerRepo
) {
    operator fun invoke(params: DropLobbyRequest.Params) =
        if (!lobbyRepo.existsById(params.id)) {
            false
        } else {
            val lobby = lobbyRepo.getById(params.id)

            if (lobby == null) {
                false
            } else {
                lobby.players.forEach { playerRepo.delete(it) }

                lobbyRepo.delete(lobby)

                true
            }
        }
}