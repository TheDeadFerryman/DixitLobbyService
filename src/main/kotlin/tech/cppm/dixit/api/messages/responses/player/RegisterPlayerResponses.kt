package tech.cppm.dixit.api.messages.responses.player

import tech.cppm.dixit.db.documents.Player
import tech.cppm.dixit.api.messages.responses.ErrorCode
import tech.cppm.dixit.api.messages.responses.ErrorResponse
import tech.cppm.dixit.api.messages.responses.SuccessResponse

sealed class RegisterPlayerResponses {
    class RegisterSuccess(
        result: Player,
        id: String?
    ) : SuccessResponse<Player>(result, id)

    class ErrorPlayerExists(
        id: String?
    ) : ErrorResponse(
        id, error = ApiError(ErrorCode.REGISTER_PLAYER_EXISTS)
    )
}
