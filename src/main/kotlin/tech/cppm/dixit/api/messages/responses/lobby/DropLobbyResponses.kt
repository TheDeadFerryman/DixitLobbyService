package tech.cppm.dixit.api.messages.responses.lobby

import tech.cppm.dixit.api.messages.responses.ErrorCode
import tech.cppm.dixit.api.messages.responses.ErrorResponse
import tech.cppm.dixit.api.messages.responses.SuccessResponse

sealed class DropLobbyResponses {
    class DroppedSuccess(
        id: String?
    ) : SuccessResponse<Nothing?>(
        result = null, id
    )

    class ErrorInvalidId(
        id: String?
    ) : ErrorResponse(
        id,
        error = ApiError(
            code = ErrorCode.DROP_LOBBY_INVALID_ID
        )
    )
}