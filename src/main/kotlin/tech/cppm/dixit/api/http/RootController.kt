package tech.cppm.dixit.api.http

import com.fasterxml.jackson.databind.exc.InvalidTypeIdException
import com.fasterxml.jackson.databind.exc.MismatchedInputException
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import org.slf4j.LoggerFactory
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import tech.cppm.dixit.api.ApiHandler
import tech.cppm.dixit.util.KReflect
import tech.cppm.dixit.api.messages.ApiRequest
import tech.cppm.dixit.api.messages.responses.ErrorResponse
import tech.cppm.dixit.api.messages.responses.internal.*
import java.lang.Exception
import java.lang.NullPointerException

@RestController
class RootController(
    val apiHandler: ApiHandler
) {
    private val logger = LoggerFactory.getLogger(this::class.java)

    @PostMapping("/api")
    fun handle(@RequestBody request: ApiRequest<*>) =
         try {
            apiHandler.handle(request)
        } catch (e: Exception) {
            logger.error(e.message, e)
            InternalErrorResponse(data = mapOf(
                "cause" to e.javaClass.kotlin.simpleName
            ))
        }

    @ExceptionHandler(HttpMessageNotReadableException::class)
    fun handleInvalidRequest(
        exception: HttpMessageNotReadableException
    ): ErrorResponse {
        logger.warn(exception.message)

        return when (val cause = exception.cause) {
            is MissingKotlinParameterException -> handleMissingParameter(cause)
            is InvalidTypeIdException -> NoMethodResponse(cause.typeId)
            is MismatchedInputException -> handleMismatchedInput(cause)
            else -> InvalidJSONResponse()
        }
    }

    @ExceptionHandler(NullPointerException::class)
    fun handleNullPointerAccess(exception: NullPointerException) = InternalErrorResponse(
        data = mapOf("cause" to "NullPointerAccess")
    )

    private fun handleMismatchedInput(cause: MismatchedInputException): ErrorResponse {
        if (cause.path.isNotEmpty()
            && cause.path.first().fieldName == "params"
            && (cause.path.size > 1)
        ) {
            val causePath = cause.path
                .takeLast(cause.path.size - 1)
                .joinToString(".") { it.fieldName }

            return InvalidParamsResponse(
                data = mapOf(
                    "cause" to "MismatchedInput",
                    "location" to causePath,
                    "expectedType" to cause.targetType.simpleName,
                )
            )
        }

        return InvalidRequestResponse(
            data = hashMapOf(
                "cause" to "MismatchedInput",
                "location" to cause.path.joinToString(".") { it.fieldName },
                "expectedType" to cause.targetType?.simpleName
            )
        )
    }

    private fun handleMissingParameter(cause: MissingKotlinParameterException): ErrorResponse {
        if (cause.path.first().fieldName == "params"
            && (cause.path.size > 1)
        ) {
            val causePath = cause.path
                .takeLast(cause.path.size - 1)
                .joinToString(".") { it.fieldName }

            return InvalidParamsResponse(
                data = mapOf(
                    "cause" to "MissingParameter",
                    "location" to causePath,
                    "expectedType" to KReflect.publicTypeName(cause.parameter.type)
                )
            )
        }

        return InvalidRequestResponse(
            data = hashMapOf(
                "cause" to "MissingParameter",
                "location" to cause.path.joinToString(".") { it.fieldName },
                "expectedType" to KReflect.publicTypeName(cause.parameter.type)
            )
        )
    }
}