package tech.cppm.dixit.control.messages.commands

import com.fasterxml.jackson.annotation.JsonTypeName
import tech.cppm.dixit.control.messages.ControlMessage
import tech.cppm.dixit.db.documents.Player

@JsonTypeName("control.players.onGameStarted")
class GameStartedCommand(
    params: Params
) : ControlMessage<GameStartedCommand.Params>(
    method = "control.players.onGameStarted",
    params
) {
    data class Params(
        val players: List<Player>,
        val seed: Int
    ) : ControlMessage.Params()
}