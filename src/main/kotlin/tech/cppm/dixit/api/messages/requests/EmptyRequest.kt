package tech.cppm.dixit.api.messages.requests

import com.fasterxml.jackson.annotation.JsonTypeName
import tech.cppm.dixit.api.messages.ApiRequest

@JsonTypeName("$$.empty")
class EmptyRequest(
    id: String?
) : ApiRequest<EmptyRequest.Params>(
    method = "$$.empty",
    params = Params(),
    id
) {
    class Params : ApiRequest.Params()
}