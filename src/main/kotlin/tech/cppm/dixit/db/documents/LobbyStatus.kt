package tech.cppm.dixit.db.documents

enum class LobbyStatus {
    CREATED,
    PLAYING,
    FORSAKEN
}