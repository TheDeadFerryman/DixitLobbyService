package tech.cppm.dixit.db.repo

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import tech.cppm.dixit.db.documents.Player

@Repository
interface PlayerRepo : MongoRepository<Player, String> {
    fun findByAliasAndPassword(alias: String, password: String): Player?

    fun existsByAlias(alias: String): Boolean
}