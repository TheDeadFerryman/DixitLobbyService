package tech.cppm.dixit.api.messages.requests

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside
import com.fasterxml.jackson.annotation.JsonSubTypes
import tech.cppm.dixit.api.messages.requests.lobby.*
import tech.cppm.dixit.api.messages.requests.player.RegisterPlayerRequest
import tech.cppm.dixit.control.messages.requests.EndGameRequest
import tech.cppm.dixit.control.messages.requests.StartGameRequest

@JacksonAnnotationsInside
@JsonSubTypes(
    JsonSubTypes.Type(FindLobbyRequest::class),
    JsonSubTypes.Type(CreateLobbyRequest::class),
    JsonSubTypes.Type(JoinLobbyRequest::class),

    JsonSubTypes.Type(RegisterPlayerRequest::class),
    JsonSubTypes.Type(EmptyRequest::class),

    JsonSubTypes.Type(StartGameRequest::class),
    JsonSubTypes.Type(EndGameRequest::class)
)
annotation class RequestTypes
