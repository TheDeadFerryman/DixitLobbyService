package tech.cppm.dixit.control.amqp

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Service
import tech.cppm.dixit.api.messages.ApiRequest
import tech.cppm.dixit.config.AMQPConfig
import tech.cppm.dixit.control.handlers.ControlHandler
import tech.cppm.dixit.control.messages.ControlMessage

@Service
class ControlListener(
    val objectMapper: ObjectMapper,
    val controlHandler: ControlHandler,
) {
    @RabbitListener(queues = [AMQPConfig.CONTROL_INCOME_QUEUE])
    fun handle(data: String) =
        when (val request = objectMapper.readValue(data, ApiRequest::class.java)) {
            is ControlMessage -> controlHandler.handle(request)
            else -> Unit
        }

}