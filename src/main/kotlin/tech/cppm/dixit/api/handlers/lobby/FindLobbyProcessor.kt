package tech.cppm.dixit.api.handlers.lobby

import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import tech.cppm.dixit.db.documents.Lobby
import tech.cppm.dixit.db.repo.LobbyRepo
import tech.cppm.dixit.api.messages.requests.lobby.FindLobbyRequest

@Service
class FindLobbyProcessor(
    val lobbyRepo: LobbyRepo
) {
    operator fun invoke(params: FindLobbyRequest.Params): Iterable<Lobby> {
        val pager = params.page?.let { PageRequest.of(
            params.page.page,
            params.page.pageSize
        ) }

        return if(params.query.isEmpty()) {
            if (params.region == null) {
                if (pager == null) {
                    lobbyRepo.findAll()
                } else {
                    lobbyRepo.findAll(pager)
                }
            } else {
                if (pager == null) {
                    lobbyRepo.findAllByRegion(params.region)
                } else {
                    lobbyRepo.findAllByRegion(params.region, pager)
                }
            }
        } else {
            if (params.region == null) {
                if (pager == null) {
                    lobbyRepo.findAllByNameContains(params.query)
                } else {
                    lobbyRepo.findAllByNameContains(params.query, pager)
                }
            } else {
                if (pager == null) {
                    lobbyRepo.findAllByRegionAndNameContains(params.region, params.query)
                } else {
                    lobbyRepo.findAllByRegionAndNameContains(params.region, params.query, pager)
                }
            }
        }
    }
}