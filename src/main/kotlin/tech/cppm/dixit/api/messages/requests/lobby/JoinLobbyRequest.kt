package tech.cppm.dixit.api.messages.requests.lobby

import com.fasterxml.jackson.annotation.JsonTypeName
import tech.cppm.dixit.api.messages.ApiRequest
import tech.cppm.dixit.api.messages.requests.LobbyRequest

@JsonTypeName("lobbies.join")
class JoinLobbyRequest(
    params: Params,
    id: String?
) : LobbyRequest<JoinLobbyRequest.Params>(
    method = "lobbies.join",
    params, id
) {
    data class Params(
        val lobbyId: String,
        val passkey: String?,
        val playerId: String
    ) : ApiRequest.Params()
}