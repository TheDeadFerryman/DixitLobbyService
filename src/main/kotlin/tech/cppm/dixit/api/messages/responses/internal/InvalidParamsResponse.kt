package tech.cppm.dixit.api.messages.responses.internal

import tech.cppm.dixit.api.messages.responses.ErrorCode
import tech.cppm.dixit.api.messages.responses.ErrorResponse

class InvalidParamsResponse(
    data: Any? = null
) : ErrorResponse(
    id = null,
    error = ApiError(
        code = ErrorCode.PARAMS_INVALID,
        data
    )
)