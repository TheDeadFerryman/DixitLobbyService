package tech.cppm.dixit.api.messages.responses

class EmptyResponse(
    id: String?
) : SuccessResponse<Unit?>(
    result = null,
    id
)