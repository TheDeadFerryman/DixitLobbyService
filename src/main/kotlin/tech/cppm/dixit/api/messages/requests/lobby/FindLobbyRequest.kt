package tech.cppm.dixit.api.messages.requests.lobby

import com.fasterxml.jackson.annotation.JsonTypeName
import tech.cppm.dixit.api.messages.ApiRequest
import tech.cppm.dixit.api.messages.responses.Page
import tech.cppm.dixit.api.messages.requests.LobbyRequest
import tech.cppm.dixit.db.documents.LobbyRegion

@JsonTypeName("lobbies.find")
class FindLobbyRequest(
    params: Params,
    id: String?,
) : LobbyRequest<FindLobbyRequest.Params>(
    method = "lobbies.find",
    params, id
) {
    data class Params(
        val query: String,
        val region: LobbyRegion?,
        val page: Page?
    ) : ApiRequest.Params()
}