package tech.cppm.dixit.api.messages.responses.internal

import tech.cppm.dixit.api.messages.responses.ErrorCode
import tech.cppm.dixit.api.messages.responses.ErrorResponse

class InvalidJSONResponse : ErrorResponse(
    error = ApiError(
        code = ErrorCode.PARSE_FAILED,
    ),
    id = null
)