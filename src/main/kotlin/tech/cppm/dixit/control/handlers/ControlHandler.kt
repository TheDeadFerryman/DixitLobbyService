package tech.cppm.dixit.control.handlers

import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import tech.cppm.dixit.control.amqp.AMQPEntityManager
import tech.cppm.dixit.control.amqp.ControlDispatcher
import tech.cppm.dixit.control.messages.ControlMessage
import tech.cppm.dixit.control.messages.commands.GameStartedCommand
import tech.cppm.dixit.control.messages.requests.EndGameRequest
import tech.cppm.dixit.control.messages.requests.StartGameRequest
import tech.cppm.dixit.db.documents.LobbyStatus
import tech.cppm.dixit.db.repo.LobbyRepo
import tech.cppm.dixit.db.repo.PlayerRepo

@Service
class ControlHandler(
    private val lobbyRepo: LobbyRepo,
    private val playerRepo: PlayerRepo,
    private val dispatcherFactory: ControlDispatcher.Factory
) {
    class InvalidLobbyException : Exception()
    class InvalidUserException : Exception()
    class UnauthorizedException : Exception()

    fun handle(message: ControlMessage<*>) =
        when(message) {
            is StartGameRequest -> handleStart(message)
            is EndGameRequest -> handleEnd(message)
            else -> Unit
        }

    fun handleStart(request: StartGameRequest) {
        val lobby = lobbyRepo.findByIdOrNull(request.params.lobbyId) ?: throw InvalidLobbyException()
        val player = playerRepo.findByIdOrNull(request.params.creatorId) ?: throw InvalidUserException()

        val dispatcher = dispatcherFactory.forLobby(lobby)

        lobby.players.firstOrNull { it.id == player.id } ?: throw UnauthorizedException()

        lobbyRepo.save(
            lobby.setStatus(LobbyStatus.PLAYING)
        )

        dispatcher.sendBroadcast(GameStartedCommand(
            params = GameStartedCommand.Params(
                players = lobby.players,
                seed = lobby.seed
            )
        ))
    }

    fun handleEnd(request: EndGameRequest) {
        val lobby = lobbyRepo.findByIdOrNull(request.params.lobbyId) ?: throw InvalidLobbyException()
        val player = playerRepo.findByIdOrNull(request.params.playerId) ?: throw InvalidUserException()

        lobby.players.firstOrNull { it.id == player.id } ?: throw UnauthorizedException()

        when {
            lobby.status != LobbyStatus.FORSAKEN -> {
                lobbyRepo.save(
                    lobby
                        .setStatus(LobbyStatus.FORSAKEN)
                        .removePlayer(player)
                )
            }
            lobby.players.isEmpty() -> {
                lobbyRepo.delete(lobby)
            }
            else -> {
                lobbyRepo.save(
                    lobby.removePlayer(player)
                )
            }
        }
    }
}