package tech.cppm.dixit.api.messages.responses.lobby

import tech.cppm.dixit.api.messages.responses.ErrorCode
import tech.cppm.dixit.api.messages.responses.ErrorResponse
import tech.cppm.dixit.api.messages.responses.SuccessResponse

sealed class CreateLobbyResponses {
    class CreatedSuccess(
        result: String,
        id: String?
    ) : SuccessResponse<String>(result, id)

    class LobbyAlreadyExistsError(
        id: String?
    ) : ErrorResponse(
        id, error = ApiError(
            code = ErrorCode.CREATE_LOBBY_EXISTS,
        )
    )

    class InvalidPlayerError(
        id: String?
    ) : ErrorResponse(
        id, error = ApiError(
            code = ErrorCode.CREATE_LOBBY_INVALID_PLAYER,
        )
    )

}