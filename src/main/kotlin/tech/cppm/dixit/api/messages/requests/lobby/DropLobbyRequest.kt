package tech.cppm.dixit.api.messages.requests.lobby

import com.fasterxml.jackson.annotation.JsonTypeName
import tech.cppm.dixit.api.messages.ApiRequest
import tech.cppm.dixit.api.messages.requests.LobbyRequest

@JsonTypeName("lobbies.create")
class DropLobbyRequest(
    params: Params,
    id: String?
) : LobbyRequest<DropLobbyRequest.Params>(
    method = "lobbies.create",
    params, id
) {
    data class Params(
        val id: String,
    ) : ApiRequest.Params()
}