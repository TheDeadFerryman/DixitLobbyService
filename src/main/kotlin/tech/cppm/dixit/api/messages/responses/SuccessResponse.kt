package tech.cppm.dixit.api.messages.responses

import tech.cppm.dixit.api.messages.ApiResponse

abstract class SuccessResponse<ResultType>(
    val result: ResultType,
    id: String?
) : ApiResponse(id)